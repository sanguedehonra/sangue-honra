﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Menu : MonoBehaviour {


	bool subindo = false;
	bool bolsa_up;
	float px, py;
	Coroutine co;

	string panel_subir;

	void Update()
	{
		if (subindo) 
		{
			GameObject cf = GameObject.Find (panel_subir).gameObject;
			cf.transform.position = Vector2.Lerp (cf.transform.position, new Vector2 (px, py), 0.2f);
		}
	}

	void Start()
	{
		Time.timeScale = 1;
	}

	public void back_to_pmenu()
	{
		if (Camera.main.transform.position.y == 0)
		{
			Application.LoadLevel ("menu");
		}
	}

	public void clicou_novo_jogo(float pos)
	{
		//gameObject.SetActive (false);
		GameObject cam = GameObject.Find ("Main Camera");
		cam.transform.position = new Vector3 (0, pos, -10);

		/*GameObject painel_fases = GameObject.Find("Canvas");
		painel_fases.gameObject.transform.FindChild ("Fases").gameObject.SetActive (true);*/

	}

	public void click_fase(string n)
	{
		Application.LoadLevel (n);
	}

	public void sair_jogo()
	{
		Application.Quit ();
	}

	void click_inicial(bool novo_jogo)
	{
		
	}


	public void subir_panel(string panel)
	{
		panel_subir = panel;
		px = 0;
		py = 0;
		subindo = true;
	}

	public void descer_panel()
	{
		py = -10;
	}

	public void subir_bolsa(string bolsa)
	{
		if (!bolsa_up)
		{
			GameObject.Find ("EventSystem").GetComponent<Controlador_torre> ().click_terreno ();
			panel_subir = "bolsa";
			px = 6.7f;
			py = -1.1f;
			subindo = true;
			bolsa_up = true;
		} 

		else 
		{
			descer_bolsa ();
		}
	}

	public void descer_bolsa()
	{
		py = -8;
		bolsa_up = false;
	}
		

	public void volume_control(string q)
	{
		GameObject audio = GameObject.Find (q);
		audio.gameObject.GetComponent<AudioSource>().volume = GameObject.Find (q + "_slider").gameObject.GetComponent<Slider> ().value;
	}
		
	public IEnumerator popup_lore(GameObject content_panel)
	{
		content_panel.gameObject.transform.localScale = (new Vector2 (content_panel.gameObject.transform.localScale.x + 1f, content_panel.gameObject.transform.localScale.y + 1f));
		//content_panel.gameObject.transform.localScale = new Vector2(6.5f, 6.5f);
		if (content_panel.gameObject.transform.localScale.x >= 7f)
		{
			Debug.Log ("Testador");
			yield break; //StopCoroutine ("popup_lore");
		}
		yield return new WaitForSeconds(0.00001f);
		StartCoroutine ("popup_lore", content_panel);
	}

	public void ativar_popup_lore(string cp)
	{
		GameObject content_panel = GameObject.Find (cp);
		content_panel.gameObject.transform.localScale = new Vector2(6.5f, 6.5f);
	}
		
}
