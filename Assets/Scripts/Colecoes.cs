﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public  class Colecoes :MonoBehaviour {

	public static List<GameObject> caminhos;
    public static List<GameObject> esquadrao;
    public static List<GameObject> unidades_disponiveis;
    public static List<GameObject> botoes_unidade;
    public static List<GameObject> botoes_esquadrao;

	void Start(){
        esquadrao = new List<GameObject>();
		caminhos = new List<GameObject>( GameObject.FindGameObjectsWithTag ("Caminho"));// procura todos os caminhos da cena
        botoes_unidade = new List<GameObject>();
	}
    void Update(){

        esquadrao.Exists(item => item == gameObject);   
    }

    public static bool existe_esquadrao(GameObject esquad) // verifica se um esquadrao existe na lista
    {
        if (esquad == null)
            return false;
        foreach (GameObject esq in esquadrao)
        {
            if (esq == esquad)
                return true;
        }
        return false;
    }
}
