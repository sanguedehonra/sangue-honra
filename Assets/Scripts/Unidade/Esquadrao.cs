﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Esquadrao : MonoBehaviour {
	public List<GameObject> unidades;
	public List<GameObject> pseudo_caminho;

   
    GameObject controladorjogo;
    public GameObject caminho_esquadrao; 
    public string fei;
    Ray raio;
    Ray raio2;
	// Use this for initialization
	void Start () {
        //controladorjogo = GameObject.FindGameObjectWithTag("controlador");

		/*foreach (GameObject c in transform) 
		{
			unidades.Add (c.gameObject);
		}*/
	}
	
	// Update is called once per frame
	void Update () {

   
        if (Input.GetMouseButton (0))  // botao direito do mouse
        { 
         //   Debug.Log ("definindo caminho");
            Definircaminho ();
        } 
        else if (Input.GetMouseButtonUp (0) && pseudo_caminho != null)
            pseudo_caminho.Clear ();

	}
    public void Definircaminho ()
    {
        

        raio = Camera.main.ScreenPointToRay (Input.mousePosition);
        GameObject colisor;

        if (Physics2D.Raycast (raio.origin, raio.direction))
        {
            colisor = Physics2D.Raycast (raio.origin, raio.direction).collider.gameObject;
            if (colisor.tag == "ponto_inicial" && pseudo_caminho.Count == 0)
            {
                // começo da definiçao do caminho
                Debug.Log("definindo caminho");
                pseudo_caminho.Add (colisor); // adiciona o ponto inicial ao caminho;
                Debug.Log (pseudo_caminho [0].name);
            } 
            else if (colisor.tag == "ponto" && pseudo_caminho.Count != 0 && Validar_caminho (pseudo_caminho, colisor) && !pseudo_caminho.Contains (colisor)) // adiciona algum ponto intermediario e verifica se ele e valido
            {
                Debug.Log ("novo ponto:" + Physics2D.Raycast (raio.origin, raio.direction).collider.name);
                pseudo_caminho.Add (colisor);
            } 
            else if (colisor.tag == "ponto_final" && !pseudo_caminho.Contains (colisor))//se chegar no ultimo ponto
            {
                pseudo_caminho.Add (colisor);
                foreach (GameObject c in Colecoes.caminhos) 
                {
                    if (pseudo_caminho.Contains (c.GetComponent<Caminho> ().Getpontofinal ())) // encontra
                    {
                        Debug.Log ("caminho setado ");
                        caminho_esquadrao = c; 
                        set_caminho_unidade();
                        break;
                    }
                }
            }
        }

    }

     bool Validar_caminho (List<GameObject> pseu_caminho, GameObject ponto)
    {
        pseu_caminho = new List<GameObject> ();
        int caminho_invalido = 0; // contador de caminhos invalidos
        pseu_caminho.Add (ponto); // adiciona o ponto a ser verificado ao o pseudo pseudo caminho
        foreach (GameObject c in Colecoes.caminhos) { // verifica todos os caminhos
            foreach (GameObject p in pseu_caminho) { // pecorre todo o pseudo pseudo caminho
                if (!c.GetComponent<Caminho> ().Getpontoscaminho ().Contains (p)) { // caso o ponto nao exista no caminho esse caminho nao e o que esta sendo selecionado
                    caminho_invalido++;
                    break;
                }
            }
        }
        Debug.Log ("caminhos invalidos " + caminho_invalido);
        if (caminho_invalido == Colecoes.caminhos.Count) // se o numero de caminhos invalidos for igual ao total de caminhos entao conjunto do pontos invalidos
            return false;
        return true;
    }

    void set_caminho_unidade ()
    {
        foreach (GameObject unidade in unidades)
        {
            unidade.GetComponent<Unidade>().caminho_unidade = caminho_esquadrao;
            unidade.GetComponent<Unidade>().proximo_ponto = caminho_esquadrao.GetComponent<Caminho>().Getpontoinicial();
        }
    }
  


    public void remover_unidade(GameObject unidade)
    {
        
    }
}
