﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class unidade_battle : MonoBehaviour {

	public bool apareceu = false;
	float vel;
	int estado;

	enum Estado
	{
		parado,
		andando,
		atacando
	};

	void andar_batalha()
	{
		gameObject.transform.Translate(vel * Time.deltaTime, 0, 0);
	}

	IEnumerator atacar_batalha (GameObject at)
	{
		Dano.dar_dano (at, gameObject.GetComponent<Unidade_normal> ().ataque, 0, 0);
		yield return new WaitForSeconds(gameObject.GetComponent<Unidade_normal> ().velocidade_atque);
		StartCoroutine ("atacar_batalha", at);
	}

	// Use this for initialization
	void Start () {
		estado = (int)Estado.andando;
		vel = gameObject.GetComponent<Unidade_normal> ().velocidade;
		if (tag != "Aliada")
			vel = vel * -1;

	}
	
	// Update is called once per frame
	void Update ()
	{

		if(estado == (int)Estado.andando) andar_batalha ();


		if (gameObject.GetComponent<Renderer> ().isVisible)
			apareceu = true;

		if (!gameObject.GetComponent<Renderer> ().isVisible && apareceu)
		{
			GameObject g = GameObject.Find ("EventSystem");
			if (tag == "Aliada") 
			{
				g.GetComponent<Controlador_battle> ().vida_inimigo -= gameObject.GetComponent<Unidade_normal> ().dano_vida;
				g.GetComponent<Controlador_battle> ().vida_inimigo_text.text = g.GetComponent<Controlador_battle> ().vida_inimigo.ToString ();
			} 

			else
			{
				g.GetComponent<Controlador_battle> ().vida_jogador -= gameObject.GetComponent<Unidade_normal> ().dano_vida;
				g.GetComponent<Controlador_battle> ().vida_jogador_text.text = g.GetComponent<Controlador_battle> ().vida_inimigo.ToString ();
			}
			Destroy (gameObject);
		}


	}

	void OnTriggerEnter2D(Collider2D chegador)
	{
		if (chegador.tag != "aliada") 
		{
			estado = (int)Estado.atacando;
			StartCoroutine ("atacar_batalha", chegador.gameObject);
		}
	}
}
