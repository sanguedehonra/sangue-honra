﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StahlBagg : MonoBehaviour {

	public GameObject damage_area;
	public Caminho []caminhos;

	public void Special()
	{
		Caminho selected_caminho = caminhos [Random.Range (0, caminhos.Length)];
		GameObject target = selected_caminho.pontos_caminho[Random.Range(0, selected_caminho.pontos_caminho.Count-1)];

		Debug.Log ("ALVO DO SPECIAL: " + target.name);

		damage_area.transform.position = target.transform.position;
		damage_area.SetActive (true);
	}
}
