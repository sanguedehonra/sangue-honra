﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class heroi_def : Unidade {

	public bool 		selected = false;
	public bool 		eh_pra_mover_heroi = false;
	public Vector2 		ponto;
	GameObject			point_to_unidade_class;

	// Use this for initialization
	void Start () {
		point_to_unidade_class = new GameObject ();
		point_to_unidade_class.name = "point_to_unidade_class";
	}
	
	// Update is called once per frame
	void Update () {
		if (eh_pra_mover_heroi)
		{
			Debug.Log ("Selected: " + selected);
			point_to_unidade_class.transform.position = ponto;
			Andar (point_to_unidade_class);

			if (this.ponto.x == transform.position.x && this.ponto.y == transform.position.y)
			{
				eh_pra_mover_heroi = false;
			}
		}

		base.Update ();
			
	}

	public void selecionar_hero()
	//void OnMouseDown()
	{
		selected = true;
	}

	public void click_path()
	{
		Debug.Log ("FORA DA FUNÇÂO FOI.");
		if (selected)
		{
			Debug.Log ("DENTRO TAMBÉM.");
			eh_pra_mover_heroi = true;
			Ray2D ray = new Ray2D (Camera.main.ScreenPointToRay (Input.mousePosition).origin, Camera.main.ScreenPointToRay (Input.mousePosition).direction);
			ponto = ray.origin + ray.direction;
			//ponto.y = ray.origin + ray.direction;
			selected = false;
		}
	}
}
