﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class unidade_quartel : Unidade {

	public bool move = false;
	public Vector2 ponto;
	public GameObject parsa1 = null;
	public GameObject parsa2 = null;

	// Use this for initialization
	void Start () {
		state = State.STOPED;

		Transform units = transform.parent;
		foreach (Transform u in units)
		{
			if (u != transform)
			{
				if (parsa1 == null)
					parsa1 = u.gameObject;
				else
					parsa2 = u.gameObject;
			}
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
	}

	public void Andar (Vector2 ponto)
	{
		Debug.Log ("ENENI EREZA FOREBILON");
		float step = gameObject.GetComponent<Unidade> ().velocidade * Time.deltaTime;
		transform.position = Vector2.MoveTowards (transform.position, ponto, step);

	}

}
