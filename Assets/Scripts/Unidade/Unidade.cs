﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public abstract class Unidade : MonoBehaviour
{
	public int ataque;
	public int dano_vida;
	public int vida;
	public int def;
	public int def_mg;
	public float velocidade;
	public float velocidade_atque;
	public int preco;
	public GameObject proximo_ponto;
	public GameObject atual_ponto;
	public GameObject caminho_unidade;
	public GameObject controlador_jogo;
    public GameObject esquadrao;
	public List<GameObject> pseudo_caminho;
    Vector2 posicao_antes_arrastar;
	Ray raio;
	bool tela_wave;
	GameObject colidido = null;
    public GameObject area_esquadrao;
	public GameObject atack_target = null;

	controlador_wave cw;
	public State state = State.WALKING;

	public Animator anime;

	public enum State : byte
	{
		WALKING,
		STOPED,
		ATACKING,
		DEAD,
	}

	// Use this for initialization
     void  Start () 
	{
        Debug.Log("sdfg");
        area_esquadrao = GameObject.FindGameObjectWithTag("area_esquadrao_criacao");

        //controlador_jogo = GameObject.FindGameObjectWithTag("controlador");

		anime = this.GetComponent<Animator> ();

		cw = GameObject.Find ("EventSystem").GetComponent<controlador_wave> ();
	}
	
	// Update is called once per frame
	public virtual void Update ()
	{
		if (vida <= 0 || state == State.DEAD)
		{
			if (state != State.DEAD && gameObject.tag == "Inimigo")
				cw.unit_quant--;
			anime.SetBool ("morte_b", true);
			anime.SetBool ("morto_b", true);
			gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			Destroy (gameObject.GetComponent<Rigidbody2D> ());
			state = State.DEAD;
		}

		else if (state == State.ATACKING)
		{
			Debug.Log (gameObject.name + ", " + state + "SATANAIS!!!");
			anime.SetBool ("atacking", true);
		}

		else
		{
			if (caminho_unidade != null && state == State.WALKING)
				Andar (proximo_ponto);
		}
     
		
	}

	public virtual void Andar (GameObject ponto)
	{
		transform.position = Vector2.MoveTowards (transform.position, ponto.transform.position, velocidade * Time.deltaTime);// translação para o proximo ponto
		if (ponto.transform.position.y - atual_ponto.transform.position.y < ponto.transform.position.x - atual_ponto.transform.position.x)
		{
			anime.SetFloat ("posy", -1f);
			anime.SetFloat ("posx", 0f);
		}

		else if (ponto.transform.position.y - atual_ponto.transform.position.y > ponto.transform.position.x - atual_ponto.transform.position.x)
		{
			if (ponto.transform.position.x - atual_ponto.transform.position.x > 0)
			{
				anime.SetFloat ("posy", 0f);
				anime.SetFloat ("posx", 1f);
				anime.SetFloat ("atack", 1f);
			}

			else
			{
				anime.SetFloat ("posy", 0f);
				anime.SetFloat ("posx", -1f);
				anime.SetFloat ("atack", 0f);
			}
		}
	}
        
	public virtual void Defender ()
	{
	}

	IEnumerator intervalo_ataque()
	{
		yield return new WaitForSeconds (velocidade);
		Atacar ();
		StartCoroutine (intervalo_ataque());
	}

	public virtual void Atacar ()
	{
		Dano.dar_dano (atack_target, ataque, 0, 0);
	}

	
	public virtual  void Setproximo_ponto (GameObject proximo_ponto)
	{ 
		this.proximo_ponto = proximo_ponto;
	}

	void OnTriggerEnter2D (Collider2D col) // colisao
	{
		if (col.tag == "ponto")
		{
			colidido = col.gameObject;
			if (caminho_unidade != null)
			{
				caminho_unidade.GetComponent<Caminho> ().Muda_ponto (this.gameObject, col.gameObject); //  verificacao a interação do unidade com o ponto
				atual_ponto = col.gameObject;
			}

		}

		else if ((gameObject.tag == "Aliada" && col.tag == "Inimigo"))
		{
			if (atack_target == null && col.gameObject.GetComponent<Unidade>().atack_target == null)
			{
				state = State.ATACKING;
				col.GetComponent<Unidade> ().state = State.ATACKING;
				atack_target = col.gameObject;
				col.gameObject.GetComponent<Unidade> ().atack_target = gameObject;

				StartCoroutine (intervalo_ataque());
			}

			Debug.Log (state + ", " + col.GetComponent<Unidade> ().state);
		}
      
	}

	void OnTriggerExit2D(Collider2D col)
	{
		if (col.gameObject == atack_target /*&& gameObject.tag == "Inimigo"*/)
		{
			Debug.Log ("SAIUUUUUUU!!!!!");
			if (gameObject.tag == "Inimigo") state = State.WALKING;
			else if (gameObject.tag == "Aliada") state = State.STOPED;
			atack_target = null;
			StopCoroutine (intervalo_ataque());
		}
	}

}