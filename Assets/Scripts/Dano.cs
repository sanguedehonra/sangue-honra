﻿using UnityEngine;
using System.Collections;

public class Dano : MonoBehaviour {

	public int tipo;

	public static void dar_dano(GameObject atacado, int ataque, int buff, int tipo)
	{
		if (atacado != null)
		{
			float dano_final; 
			switch(tipo)
			{
			case 0: 
				dano_final = ataque - (ataque * atacado.gameObject.GetComponent<Unidade> ().def  / 100);
				atacado.gameObject.GetComponent<Unidade> ().vida -= Mathf.RoundToInt (dano_final);
				Debug.Log ("Dano: "+ Mathf.RoundToInt (dano_final));
				break;

			case 1: 
				dano_final = ataque - (ataque * atacado.gameObject.GetComponent<Unidade> ().def_mg  / 100);
				atacado.gameObject.GetComponent<Unidade> ().vida -= Mathf.RoundToInt(dano_final);
				break;
			}
		}



	}


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
