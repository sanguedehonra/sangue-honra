﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Compra : MonoBehaviour {

	public int grana; //var de testes.
	static GameObject selected_but;
	int aux = 0;
	int nivel_t1 = 1;
	int nivel_t2 = 1;
	int nivel_t3 = 1;

	int nivel_u1 = 1;
	int nivel_u2 = 1;
	int nivel_u3 = 1;

	public void selecionar(string b_nome)
	{
		selected_but = GameObject.Find (b_nome).gameObject;
	}

	public void att_grana_tela()
	{
		GameObject.Find ("grana_quant").gameObject.GetComponent<Text> ().text = "Grana: " + grana;
	}

	void Start()
	{
	}

	void but_compra(string t, int valor)
	{
		if (grana >= valor) 
		{
			if (t == "T1.") aux = nivel_t1;
			else if (t == "T2.") aux = nivel_t2;
			else if (t == "T3.") aux = nivel_t3;

			if (t == "U1.") aux = nivel_u1;
			else if (t == "U2.") aux = nivel_u2;
			else if (t == "U3.") aux = nivel_u3;


			grana -= valor;
			if(aux <= 3) GameObject.Find (t + aux).gameObject.GetComponent<Button> ().interactable = true; 

			if (t == "T1.") nivel_t1++;
			else if (t == "T2.") nivel_t2++;
			else if (t == "T3.") nivel_t3++;

			if (t == "U1.") nivel_u1++;
			else if (t == "U2.") nivel_u2++;
			else if (t == "U3.") nivel_u3++;

			att_grana_tela ();
		}
	}

	public void but_compra_t1(int valor) { but_compra ("T1.", valor); }
	public void but_compra_t2(int valor) { but_compra ("T2.", valor); }
	public void but_compra_t3(int valor) { but_compra ("T3.", valor); }

	public void but_compra_u1(int valor) { but_compra ("U1.", valor); }
	public void but_compra_u2(int valor) { but_compra ("U2.", valor); }
	public void but_compra_u3(int valor) { but_compra ("U3.", valor); }

}
