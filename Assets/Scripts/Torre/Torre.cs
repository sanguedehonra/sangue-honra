﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Torre : MonoBehaviour {

	//valores pertinentes à torre
	public int 				 tipo_dano;
	public int 				 valor_de_compra;
	public float 			 velocidade;
	public int 				 ataque;
	public int 				 buff;
	public float 			 alcance;
	public int 				 nivel;
	public CircleCollider2D  circulo_alcance;
	public GameObject 		 alvo = null;
	public GameObject		 flecha;
	public ArrayList		 alvos_da_torre = new ArrayList ();
	public static GameObject atual_torre_clicada;
	public int 				 tipo_torre;
	public Sprite 			 torre_nv2;
	public Sprite 			 torre_nv3;

	public int[] 			 up_prizes;
	public Torre[] 			 up_stats;

	Animator anime;


	//função responsável por fazer a torre atacar
	public void atacar()
	{
		if (alvo.transform.position.x > transform.position.x)
		{
			anime.SetFloat ("left", 1);
		}
		else
		{
			anime.SetFloat ("left", -1);
		}

		if (tipo_torre == 2) 
		{
			Transform area_e = transform.Find ("explosion_area").transform;
			area_e.position = alvo.transform.position;
			area_e.gameObject.SetActive (true);
		}
		Dano.dar_dano(alvo, this.gameObject.GetComponent<Torre> ().ataque, buff, tipo_dano);
	}

	//Função responsável por controlar o intervalo entre os ataques da torre
	public IEnumerator intervalo_ataque()
	{
		yield return new WaitForSeconds (velocidade);
		atacar();
		StartCoroutine (intervalo_ataque());
	}

	 //recebe o objeto que entra no alcace da torre e o seta como alvo, mas isso só acontece em caso de perda do alvo anterior
	public void travar_alvo(GameObject novo_alvo)
	{
		alvo = novo_alvo as GameObject;
	}

	//ativa/desativa o circulo da torre visivel ao usuário
	/*public void ativar_circulo()
	{
		this.gameObject.transform.FindChild ("circle").gameObject.SetActive (!this.gameObject.transform.FindChild ("circle").gameObject.activeSelf);
	}*/


	// Use this for initialization
	void Start () {
		nivel = 1;
		this.transform.Find("circle").transform.localScale = new Vector3(alcance, alcance);
		circulo_alcance = gameObject.transform.Find ("circulo").gameObject.GetComponent<CircleCollider2D> ();
		circulo_alcance.radius = alcance;

		anime = gameObject.GetComponent<Animator> ();
	}
		
	
	// Update is called once per frame
	void FixedUpdate () {
		Debug.Log (alvos_da_torre.Count);
		if (alvos_da_torre.Count > 0 && alvo.GetComponent<Unidade> ().vida <= 0) 
		{
				alvos_da_torre.Remove (alvo);
				alvo = alvos_da_torre [0] as GameObject;
				//Debug.Log ("Pegou novo alvo, Caraleo: " + alvo.name);
		}
			
		if (alvo == null)
		{
			StopCoroutine (intervalo_ataque ());
			anime.SetFloat ("left", 0);
		}
	}
}
