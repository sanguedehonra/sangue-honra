﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torre_quartel : MonoBehaviour {

	public GameObject 		bandeirinha;
	public GameObject		unit;

	// Use this for initialization
	void Start () {
		Transform c = this.transform.Find ("circle").transform;
		Vector2 v2;
		v2.x = c.position.x + 0.5f;
		v2.y = c.position.y - 0.8f;
		GameObject unit1 = Instantiate (unit, v2, Quaternion.identity, this.gameObject.transform.Find("units").transform);
		unit1.name = "quartel_unit1";
		v2.x = c.position.x;
		v2.y = c.position.y - 1.3f;
		GameObject unit2 = Instantiate (unit, v2, Quaternion.identity, this.gameObject.transform.Find("units").transform);
		unit.name = "quartel_unit2";
		v2.x = c.position.x - 0.5f;
		v2.y = c.position.y - 0.8f;
		GameObject unit3 = Instantiate (unit, v2, Quaternion.identity, this.gameObject.transform.Find("units").transform);
		unit3.name = "quartel_unit3";
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
