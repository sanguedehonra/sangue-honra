﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.UI;

public class SmoothCamra : MonoBehaviour {

	bool ir = false;
	bool voltar = false;
	public Transform target; //gameObject invisivel que a camera segue para poder se mover
	public float camSpeed; //velocidade da camera
	public int comic_size; // tamanho do quadrinho.
	int i = 0; // controlador para saber quando o quadrinho está no inicio ou no fim;
	int loc = 0; //posição da camera e relação a inicio, meio e fim da linha da HQ.

	Vector3 location1 = new Vector3(10, 0, 0); //primeiro movimento de camera. Corresponde a somente ir um pouco para a direita
	Vector3 location2 = new Vector3(-20, -9, 0); //terceiro movimento de camera. corresponde a voltar para a esquerda enquanto desce
	
	void able_button(string s, byte t) //muda o movimento que deve ser executado.
	{
		GameObject but = GameObject.Find (s).gameObject;
		but.gameObject.GetComponent<Image> ().color = new Color32 (255, 255, 255, t);
	}

	// Update is called once per frame
	void Update () 
	{
		if (target) 
		{
			transform.position = Vector3.Lerp (transform.position, target.position, camSpeed); //executa o Lerp
		}

		if (ir == true && i < comic_size) //Ao clicar troca a posição do target pela posição do próximo movimento de camera
		{
			ir = false;
			if (loc == 2) 
			{
				target.Translate (location2.x, location2.y, 0);
				loc = -1;
			}
			else target.Translate (location1.x, location1.y, 0);
			loc++;
			i++;
		}

		if (voltar == true && i > 0) //Ao clicar volta o target para a posição anterior
		{
			voltar = false;
			if (loc == 0)
			{
				target.Translate (-location2.x, -location2.y, 0);
				loc = 3;
			}
			else target.Translate (-location1.x, -location1.y, 0);
			loc--;
			i--;
		}

		if (i == 0) able_button ("seta_left", 0);
		else if (i != 0 && i != comic_size)
		{
			able_button ("seta_right", 195);
			able_button ("seta_left", 195);
			able_button ("iniciar", 0);
			GameObject.Find ("iniciar").gameObject.GetComponent<Button> ().interactable = false;
		} 

		else if (i == comic_size) 
		{
			able_button ("seta_right", 0);
			able_button ("iniciar", 255);
			GameObject.Find ("iniciar").gameObject.GetComponent<Button> ().interactable = true;
		}

	}

	void Start() 
	{
		//atual_location = location1;
	}

	public void move(bool ir_para_frente)
	{
		if (ir_para_frente)
		{
			ir = true;
			voltar = false;
		}
		else
		{
			voltar = true;
			ir = false;
		}
	}
}
