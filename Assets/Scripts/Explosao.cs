﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosao : MonoBehaviour {

	public Torre				torre;
	public CircleCollider2D		area_explosao;
	public int 					dano_colateral;
	public float				special_duration;

	float 						passed_special_time = 0;

	// Use this for initialization
	void Start () {
		//torre = transform.parent.gameObject.GetComponent<Torre> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator dar_dano_area(GameObject target)
	{
		if (passed_special_time > special_duration)
		{
			StopCoroutine (dar_dano_area (target));
			Debug.Log("ERA PRA TER PARADO!!!");
		}

		passed_special_time += 2f;
		Debug.Log ("Danado!!" + Time.time);
		Dano.dar_dano (target.gameObject, dano_colateral, 0, 0);
		yield return new WaitForSeconds (2);
		StartCoroutine (dar_dano_area (target));
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (torre != null)
		{
			if (other.gameObject != torre.alvo)
				Dano.dar_dano (other.gameObject, dano_colateral, torre.buff, torre.tipo_dano);
		}

		else
		{
			if(other.gameObject.tag == "Inimigo") StartCoroutine (dar_dano_area (other.gameObject));
		}
	}
		
}
