﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlador_wave : MonoBehaviour {

	public wave[] 	 	 waves;
	public int[] 	 	 tempo_entre_waves;
	int			    	 current_wave = 0;
	public bool 		 comecou = false;
	int			   		 wave_quant;
	public GameObject	 last_wave;
	public int 			 unit_quant;

	// Use this for initialization
	void Start () {
		wave_quant = waves.GetLength (0);
		Debug.Log (wave_quant + "WAVE QUANT");
	}
	
	// Update is called once per frame
	void Update () {
		if (comecou) 
		{
			StartCoroutine ("mandar_waves");
			comecou = false;
		}
	}

	public void comecar(bool c)
	{
		if (!comecou)
			comecou = c;


		if (current_wave <= wave_quant - 1) 
		{
			waves [current_wave].gameObject.GetComponent<wave> ().comecar = true;
			current_wave++;
		}
		
	}

	public IEnumerator mandar_waves()
	{
		Debug.Log ("começou!! " + current_wave);
		yield return new WaitForSeconds (tempo_entre_waves [current_wave-1]);
		waves [current_wave-1].gameObject.GetComponent<wave> ().comecar = true;

		Debug.Log("CURRENT WAVE" + current_wave);


		if (current_wave <= wave_quant-1) 
		{
			current_wave++;
			StartCoroutine ("mandar_waves");
			Debug.Log("CHEGOU NO FIMMMMMMMMMMM SHAUSHAUSHAUSHAU");
		}


	}
}
