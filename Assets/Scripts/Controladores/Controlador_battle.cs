﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controlador_battle : MonoBehaviour {

	public int vida_jogador;
	public int vida_inimigo;
	public int ouro;

	public Text vida_jogador_text;
	public Text vida_inimigo_text;
	public Text ouro_text;

	// Use this for initialization
	void Start () {
		vida_inimigo_text.text = vida_inimigo.ToString();
		vida_jogador_text.text = vida_jogador.ToString();
		ouro_text.text = ouro.ToString();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
