﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controlador_jogo : MonoBehaviour {

	public heroi_def		heroi;
	public GameObject 		vida_text;
	public GameObject 		grana_text;
	public GameObject 		panel_derrota;
	public int				grana;
	public int				vida;

	public bool 			derrotado = false;
	public bool 			vitorioso = false;

	public controlador_wave cw;

	public void atualizar_grana()
	{
		grana_text.GetComponent<Text> ().text = grana.ToString ();
	}

	public void atualizar_vida()
	{
		vida_text.GetComponent<Text> ().text = vida.ToString ();
	}

	public void fim_de_jogo(string fundo_text, bool venceu)
	{
		GameObject c = GameObject.Find ("Canvas").gameObject;
		foreach (Transform e in c.transform) 
		{
			if (e.name != "stats_vg" && e.name != "game_over_fundo")
				e.gameObject.SetActive (false);
		}

		panel_derrota.transform.Find ("game_over_message").transform.Find ("message").gameObject.GetComponent<Text> ().text = fundo_text;
		panel_derrota.transform.Find ("game_over_message").transform.Find ("message").gameObject.GetComponent<Text> ().fontSize = 39;
		panel_derrota.transform.position = new Vector2 (panel_derrota.transform.localPosition.x, panel_derrota.transform.localPosition.y + 455);

		if (venceu)
			vitorioso = true;
		else
			derrotado = true;

	}
		
	// Use this for initialization
	void Start () {
		grana_text.GetComponent<Text> ().text = grana.ToString ();
		vida_text.GetComponent<Text> ().text = vida.ToString ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(vida <= 0 && !derrotado) fim_de_jogo("Perdeu, Playboy!!", false);
		if (cw.unit_quant <= 0 && !vitorioso) fim_de_jogo("Venceu, seu fragmento de merda!!", true);

	}


}
