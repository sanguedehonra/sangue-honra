﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;
//using UnityEditor.Events;

public class Controlador_torre : MonoBehaviour {

	public int[] 			precos;
	public int[]			precos_up;
	GameObject				ultima_torre_comprada = null;
	GameObject				torre_feita_da_vez;
	public GameObject		atual_torre_clicada;
	public GameObject		base_torre;
	public GameObject 		last_base;
	public GameObject 		controlador;




	public void buts_compra_mostrar(GameObject base_torre)
	{
		int i = 0;
		foreach (Transform but in base_torre.transform)
		{
			if (but.name == "losangue")
			{
				but.gameObject.SetActive (true);
				continue;
			}

			but.gameObject.SetActive (true);
			
			if (precos [i] <= controlador.GetComponent<Controlador_jogo> ().grana)
				but.GetComponent<Button> ().interactable = true;
			
			else if (precos [i] > controlador.GetComponent<Controlador_jogo> ().grana)
				but.GetComponent<Button> ().interactable = false;


			i++;
		}
	}

	public void buts_upgrade_mostrar(GameObject torre_feita)
	{
		Torre torre = torre_feita.GetComponent<Torre_botao> ().torre.GetComponent<Torre> ();

		if(torre.transform.Find ("bandeirinha(Clone)"))
			torre.transform.Find ("bandeirinha(Clone)").gameObject.SetActive (true);


		foreach (Transform but in torre_feita.transform)
		{
			if (but.name == "losangue")
			{
				but.gameObject.SetActive (true);
				continue;
			}

			if (but.name == "move_units" && torre.tipo_torre == 1)
			{
				but.gameObject.SetActive (true);
				continue;
			}
			else if (but.name == "move_units" && torre.tipo_torre != 1)
			{
				but.gameObject.SetActive (false);
				continue;
			}

			but.gameObject.SetActive (true);

			if (torre.nivel < 3)
			{
				if (torre.up_prizes [torre.nivel - 1] <= controlador.GetComponent<Controlador_jogo> ().grana)
					but.GetComponent<Button> ().interactable = true;

				else if (torre.up_prizes [torre.nivel - 1] > controlador.GetComponent<Controlador_jogo> ().grana)
					but.GetComponent<Button> ().interactable = false;
			}

			else but.GetComponent<Button> ().interactable = false;
			
			if (but.name == "vender_torre")
				but.GetComponent<Button> ().interactable = true;
		}
	}

	IEnumerator delay_compra_torre(float seconds, GameObject t, Transform base_)
	{
		Debug.Log ("VAISPERÁ: " + Time.time);
		yield return new WaitForSeconds (seconds);
		ultima_torre_comprada = Instantiate (t.gameObject, new Vector3 (base_.position.x, base_.position.y - 0.5f), Quaternion.identity);
		torre_feita_da_vez.GetComponent<Torre_botao> ().torre = ultima_torre_comprada;
		torre_feita_da_vez.GetComponent<Torre_botao> ().base_t = base_.gameObject;
		torre_feita_da_vez.GetComponent<Torre_botao> ().base_t.SetActive (false);
		Debug.Log ("ISPERÔ: " + Time.time);
	}

	public void comprar_torre(GameObject t)
	{
		GameObject panel = EventSystem.current.currentSelectedGameObject.transform.Find ("torre_info").gameObject;
		GameObject[] ps = GameObject.FindGameObjectsWithTag ("panel_info");
		foreach (GameObject p in ps) 
		{

			if(p != panel)p.gameObject.SetActive (false);
		}
			
		if (panel.gameObject.activeSelf) {	
			Transform base_ = EventSystem.current.currentSelectedGameObject.transform.parent;
			StartCoroutine (delay_compra_torre(1.5f, t, base_));
			//Destroy (base_.gameObject);
			click_terreno();

			controlador.GetComponent<Controlador_jogo>().grana -= t.gameObject.GetComponent<Torre> ().valor_de_compra;
			controlador.GetComponent<Controlador_jogo> ().atualizar_grana ();
		}

		else 
		{
			panel.gameObject.SetActive (true);
		}
			
	}

	public void ativar_torre(GameObject torre_feita)
	{
		GameObject panel = EventSystem.current.currentSelectedGameObject.transform.Find ("torre_info").gameObject;
		if (panel.gameObject.activeSelf) 
		{
			torre_feita.gameObject.SetActive (true);
			torre_feita_da_vez = torre_feita;
			Debug.Log("COISA DO CAPETA SATANAIS DO INFERNO SATANAS");
		}
		/*float a = t.transform.Find("circulo").gameObject.GetComponent<CircleCollider2D> ().radius;
		t.GetComponent<SpriteRenderer> ().size = new Vector2 (a, a);*/

	}
		

	public void click_terreno ()
	{

		GameObject[] ps = GameObject.FindGameObjectsWithTag ("panel_info");
		foreach (GameObject p in ps) 
		{
			Debug.Log (p.name);
			p.gameObject.SetActive (false);
		}

		GameObject[] buts = GameObject.FindGameObjectsWithTag ("botoes_base");
		foreach (GameObject but in buts) 
		{
			but.gameObject.SetActive (false);
		}

		GameObject[] torres = GameObject.FindGameObjectsWithTag ("torre");
		foreach (GameObject t in torres) 
		{
			GameObject circle = t.gameObject.transform.Find ("circle").gameObject;

			if (t.transform.Find ("circulo").gameObject.GetComponent<Circulo> ().clicked_in_but)
			{
				t.transform.Find ("circulo").gameObject.GetComponent<Circulo> ().clicked_in_but = false;
			}

			circle.SetActive (false);
			if(t.transform.Find ("bandeirinha(Clone)"))
				t.transform.Find ("bandeirinha(Clone)").gameObject.SetActive (false);
		}
			
		if (controlador.gameObject.GetComponent<Controlador_jogo> ().heroi.selected)
		{
			/*controlador.gameObject.GetComponent<Controlador_jogo> ().heroi.eh_pra_mover_heroi = true;
			Ray2D ray = new Ray2D (Camera.main.ScreenPointToRay (Input.mousePosition).origin, Camera.main.ScreenPointToRay (Input.mousePosition).direction);
			controlador.gameObject.GetComponent<Controlador_jogo> ().heroi.ponto.transform.position = ray.origin + ray.direction;*/
			controlador.gameObject.GetComponent<Controlador_jogo> ().heroi.selected = false;
			Debug.Log ("Desselecionou o herói todo pomposo do amor: " + controlador.gameObject.GetComponent<Controlador_jogo> ().heroi.selected);
		}

		Torre.atual_torre_clicada = null;
		atual_torre_clicada = null;


	}


	public void ativar_circulo (GameObject but)
	{
		//Debug.Log("AH TEVONIOZIO AZULLLLLLLLLL " + );
		//but.gameObject.GetComponent<Torre_botao> ().torre.transform.Find ("circulo").GetComponent<SpriteRenderer> ().enabled = true;

		but.GetComponent<Torre_botao> ().torre.transform.Find ("circle").gameObject.SetActive (true);

		/*Torre.atual_torre_clicada.transform.FindChild ("circle").gameObject.SetActive (true);*/
	}

	public void upar_torre(GameObject but)
	{
		GameObject torre = but.gameObject.GetComponent<Torre_botao> ().torre;
		torre.gameObject.GetComponent<Torre> ().ataque += 8;
		torre.transform.Find("circulo").gameObject.GetComponent<CircleCollider2D>().radius += 0.3f;
		float a = torre.transform.Find ("circulo").gameObject.GetComponent<CircleCollider2D> ().radius;
		torre.gameObject.transform.Find("circle").transform.localScale = new Vector3(a, a);
		torre.gameObject.GetComponent<SpriteRenderer> ().sprite = torre.gameObject.GetComponent<Torre> ().torre_nv2;
		controlador.GetComponent<Controlador_jogo>().grana -= torre.gameObject.GetComponent<Torre>().up_prizes[torre.gameObject.GetComponent<Torre> ().nivel - 1];
		controlador.GetComponent<Controlador_jogo> ().atualizar_grana ();
		controlador.GetComponent<Controlador_jogo> ().atualizar_vida ();
		torre.gameObject.GetComponent<Torre> ().nivel++;
		click_terreno ();

	}

	public void move_units_but(GameObject torre_feita)
	{
		torre_feita.transform.Find ("losangue").gameObject.SetActive (false);

		foreach (Transform but in torre_feita.transform)
		{
			if (but.tag == "botoes_base")
				but.gameObject.SetActive (false);
		}

		//torre_feita.GetComponent<Torre_botao> ().torre.transform.Find ("circulo").GetComponent<Circulo> ().e_pra_mover = true;
		torre_feita.GetComponent<Torre_botao> ().torre.transform.Find ("circulo").GetComponent<Circulo> ().clicked_in_but = true;
	}

	public void move(GameObject t, Vector2 point)
	{
		Debug.Log ("COisa" + t.name);
		//Ray r = Camera.main.ScreenPointToRay (Input.mousePosition);
		//Vector2 point = new Vector2 (r.origin.x, r.origin.y);
		//int i = 0;
		/*foreach (Transform units in t.GetComponent<Torre> ().transform)
		{
			if (units.tag == "Aliada") 
			{
				if (i == 0)
					point.x++;
				else if (i == 1) 
				{
					point.x--;
					point.y--;
				} 
				else if (i == 2) 
				{
					point.x--;
					point.y++;
				}
				units.GetComponent<unidade_quartel> ().Andar (point);
				i++;
			}
		}*/

		t.transform.Find ("units").GetComponent<unidade_quartel> ().Andar (point);

	}

	public void vender_torre(GameObject torre_feita)
	{
		//GameObject base_t = Instantiate (base_torre, torre_feita.transform.position, Quaternion.identity, GameObject.Find ("Canvas").transform);  
		Destroy (torre_feita.GetComponent<Torre_botao>().torre.GetComponent<Torre>().gameObject);
		click_terreno ();
		torre_feita.GetComponent<Torre_botao> ().base_t.SetActive(true);
		torre_feita.transform.Find ("upar_torre").gameObject.GetComponent<Button> ().interactable = true;
		torre_feita.SetActive (false);
		click_terreno ();
		
		 

		/*UnityAction action = System.Delegate.CreateDelegate
			(typeof(UnityAction), controlador.GetComponent<Controlador_torre> (), "click_terreno") as UnityAction;
		
		UnityEventTools.AddPersistentListener (base_t.GetComponent<Button> ().onClick, action);

		action = System.Delegate.CreateDelegate
			(typeof(UnityAction), controlador.GetComponent<Controlador_torre> (), "mostrar_buts_compra") as UnityAction;
		
		UnityEventTools.AddPersistentListener (base_t.GetComponent<Button> ().onClick, action);

		foreach (Transform but in base_t)
		{
			if (but.name != "losangue") 
			{
				action = System.Delegate.CreateDelegate
					(typeof(UnityAction), controlador.GetComponent<Controlador_torre> (), "ativar_torre");
				
				UnityEventTools.AddPersistentListener (but.GetComponent<Button> ().onClick, action);

				UnityEventTools.AddObjectPersistentListener(but.GetComponent<Button>().onClick , action,  

			}
		}*/


	}

	public void desclicar_circulo(GameObject torre_feita)
	{
		/*if(torre_feita.GetComponent<Torre_botao> ().torre.GetComponent<Circulo> ().circulo_ativo)
			torre_feita.GetComponent<Torre_botao> ().torre.GetComponent<Circulo> ().circulo_ativo = false;*/
	}

}
