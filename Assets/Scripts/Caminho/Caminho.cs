﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Caminho : MonoBehaviour {
	public GameObject pontos_pai;
	public List<GameObject> pontos_caminho;
	public GameObject ponto_final;
	public GameObject ponto_inicial;
	RaycastHit2D raiohit;

	public Controlador_jogo controlador_jogo;


	// Use this for initialization
	void Start () {
		for (int i = 0; i < pontos_caminho.Count; i++)
		{
			if(i == 0) pontos_caminho[i] = pontos_pai.transform.Find ("ponto").gameObject;
			else pontos_caminho[i] = pontos_pai.transform.Find ("ponto ("+i+")").gameObject;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
		}
		

	public  void Muda_ponto(GameObject unidade,GameObject ponto_antigo )
	{
		if(unidade.GetComponent<Unidade>().proximo_ponto == ponto_antigo)
		{
        	if (ponto_antigo != ponto_final) // se nao for o ponto final
				unidade.GetComponent<Unidade>().Setproximo_ponto(pontos_caminho[pontos_caminho.IndexOf(ponto_antigo) + 1]);
        	else
        	{
				 controlador_jogo.vida -= unidade.GetComponent<Unidade>().dano_vida;
				 controlador_jogo.atualizar_vida ();
        		 Debug.Log("deu dano" + unidade.name);
        		 unidade.GetComponent<Unidade>().caminho_unidade = null;
				 Destroy (unidade);
				controlador_jogo.cw.unit_quant--;
       		 }
		}
            
	}
	public GameObject Getpontoinicial()
	{
		return ponto_inicial;	
	}
    public GameObject Getpontofinal()
	{
		return ponto_final;	
	}
	public List<GameObject> Getpontoscaminho()
	{
		return pontos_caminho;
	}
}
