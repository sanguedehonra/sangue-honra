﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wave : MonoBehaviour {

	public Esquadrao[] esquads;
	public int[] tempos_entre_squads;
	int current_squad = 0;
	int squad_quant;
	public bool comecar = false;


	// Use this for initialization
	void Start () {
		squad_quant = esquads.GetLength (0);
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log (gameObject.name + "   " + comecar);
		if (comecar)
			StartCoroutine ("mandar_squad");
	}

	IEnumerator mandar_squad()
	{
		if (current_squad <= squad_quant - 1)
		{
			esquads [current_squad].gameObject.SetActive (true);
		}

		yield return new WaitForSeconds (tempos_entre_squads [current_squad]);

		current_squad++;

		if (current_squad <= squad_quant - 1)
		{
			StartCoroutine ("mandar_squad");
			comecar = false;
			Debug.Log ("COROUTINE ENTROU NO IF DE SATANES ALADUUUUUUUUUU. " + gameObject.name);
		}
			
	}
}
