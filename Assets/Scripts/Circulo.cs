﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
//using UnityEditor.Events;
using UnityEngine.EventSystems;
using System.Linq;

public class Circulo : MonoBehaviour {

	public Torre 				torre;
	public Torre_quartel 		torre_quart;
	public Controlador_torre	controlador_torre;
	public CircleCollider2D 	circulo_torre;
	public bool 				e_pra_mover = false;
	public bool 				clicked_in_but = false;
	public Vector2				point_to_move;


	// Use this for initialization
	void Start () {
		torre = gameObject.transform.parent.gameObject.GetComponent<Torre> ();
		torre_quart = gameObject.transform.parent.gameObject.GetComponent<Torre_quartel> ();
		controlador_torre = GameObject.Find ("EventSystem").GetComponent<Controlador_torre> ();
		circulo_torre = this.gameObject.GetComponent<CircleCollider2D> ();


		//EventTrigger.Entry entry = new EventTrigger.Entry ();
		//entry.eventID = EventTriggerType.PointerDown;
		//entry.callback.AddListener ((data) => { move_units((PointerEventData)data); });
		//gameObject.GetComponent<EventTrigger> ().triggers.Add (entry);


		/*EventTrigger.TriggerEvent trigger_event = new EventTrigger.TriggerEvent ();
		trigger_event.AddListener((data) => move_units(((PointerEventData)data)));
		EventTrigger.Entry entry = new EventTrigger.Entry () { callback = trigger_event, eventID = EventTriggerType.PointerDown };
		gameObject.GetComponent<EventTrigger> ().triggers.Add (entry);*/

		//EventTrigger.Entry entry = gameObject.GetComponent<EventTrigger>().triggers.ElementAt(0);
		//entry.callback.AddListener ((data) => move_units(((PointerEventData)data)));
		//gameObject.GetComponent<EventTrigger> ().triggers.Add (entry);



		/*UnityAction action = System.Delegate.CreateDelegate
			(typeof(UnityAction), gameObject.GetComponent<Circulo>(), "move_units") as UnityAction;


		UnityEventTools.AddPersistentListener (torre.gameObject.GetComponent<EventTrigger>().OnPointerDown, action);*/


	}

	//Pega o inimigo que está dentro da área de alcance de ataque
	void OnTriggerEnter2D(Collider2D chegador)
	{
		if (chegador.tag != "circulo" && chegador.tag != "torre" && chegador.tag != "Aliada") 
		{
			if(torre.alvo == null) 
			{ 
				torre.alvo = chegador.gameObject; 
				if(torre.ataque > 0) StartCoroutine (torre.intervalo_ataque ());
			}
			torre.alvos_da_torre.Add(chegador.gameObject);

			if (torre_quart != null)
			{

				//torre.transform.Find("units").Find("quartel_unit1").gameObject;
			}


		}
	}

	//remove o objeto que sai do campo de colisão com a torre e o retira da lista de alvos. 
	//Também serve para checar se o alvo saiu do campo de colisão(saidor == alvo).
	//Caso a unidade que saiu do circulo de alcance seja o alvo da torre, essa função também setara um novo alvo para a torre.
	void OnTriggerExit2D(Collider2D saidor)
	{
		if (saidor.tag != "circulo" && saidor.tag != "torre" && saidor.tag != "Aliada") 
		{
			//Debug.Log ("Deu Exit!! " + saidor.name);
			if (saidor.gameObject == torre.alvo)
			{
				torre.alvo = null;
				if(torre.ataque > 0) StopCoroutine (torre.intervalo_ataque ());
			}

			torre.alvos_da_torre.Remove (saidor.gameObject);

			if (torre.alvo == null && torre.alvos_da_torre.Count != 0) {
				torre.travar_alvo (torre.alvos_da_torre [0] as GameObject);
				//Debug.Log ("Pegou da Fila!! Alvo = " + torre.alvo.name);
			} 
			else
			{
				StopCoroutine (torre.intervalo_ataque ());
				//Debug.Log ("Sem unidades inimigas próximas");
			}
		}
			
	}

	//void OnMouseDown()
	public void move_units()
	{
		if (clicked_in_but) 
		{
			Ray r = Camera.main.ScreenPointToRay (Input.mousePosition);
			point_to_move.x = r.origin.x;
			point_to_move.y = r.origin.y;

			Debug.Log ("POINT TO MOVE: " + point_to_move);
			Debug.Log ("TOWER POSITION: " + (Vector2)torre.transform.position); 
			Debug.Log ("DISTANCE: " + Vector2.Distance ((Vector2)torre.transform.position, point_to_move));
			Debug.Log ("RADIUS: " + torre.transform.Find ("circulo").gameObject.GetComponent<CircleCollider2D> ().radius);

			Instantiate (torre_quart.bandeirinha, point_to_move, Quaternion.identity, torre_quart.transform);
			if (Vector2.Distance ((Vector2)torre.transform.position, point_to_move) < torre.transform.Find ("circulo").gameObject.GetComponent<CircleCollider2D> ().radius+1)
			{
				if (torre_quart.transform.Find ("bandeirinha(Clone)") != null) 
					Destroy (torre_quart.transform.Find ("bandeirinha(Clone)").gameObject);
				clicked_in_but = false;
				e_pra_mover = true;
				controlador_torre.click_terreno ();
			}

			Debug.Log("TSSSSVANIAAAAAAAAAAAA");
		}

		Debug.Log("TSSSSVANIAAAAAAAAAAAATUTUTU");
	}

	void Update()
	{
		if(e_pra_mover)	
			GameObject.Find ("EventSystem").GetComponent<Controlador_torre> ().move (torre.gameObject, point_to_move);
		
		if (torre.tipo_torre == 1) 
		{
			if ((Vector2)torre.transform.Find ("units").transform.position == point_to_move && e_pra_mover)
			{
				e_pra_mover = false;
				point_to_move.x = 0;
				point_to_move.y = 0;

				Debug.Log ("FALSEOU FALSEANDO A FALSIANE DO FALSETE FALSO DE JEOVÁ");
			}

			if (Input.GetMouseButtonDown (0))
			{
				move_units ();
			}

		}



	}
}
	

